package models;

public class Wishlist {
    private int id;
    private String usertoken;
    private String name;

    public Wishlist(int id, String usertoken, String name) {
        this.id = id;
        this.usertoken = usertoken;
        this.name = name;
    }

    public String getusertoken() {
        return usertoken;
    }

    public void setusertoken(String usertoken) {
        this.usertoken = usertoken;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
