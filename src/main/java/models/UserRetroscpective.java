package models;

public class UserRetroscpective {
    private int userid;
    private String dt;
    private Double floor;
    private Double posx;
    private Double posy;

    public UserRetroscpective(int userid, String dt, Double floor, Double posx, Double posy) {
        this.userid = userid;
        this.dt = dt;
        this.floor = floor;
        this.posx = posx;
        this.posy = posy;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public Double getFloor() {
        return floor;
    }

    public void setFloor(Double floor) {
        this.floor = floor;
    }

    public Double getPosx() {
        return posx;
    }

    public void setPosx(Double posx) {
        this.posx = posx;
    }

    public Double getPosy() {
        return posy;
    }

    public void setPosy(Double posy) {
        this.posy = posy;
    }
}
