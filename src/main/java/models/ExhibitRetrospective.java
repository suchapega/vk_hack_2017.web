package models;

public class ExhibitRetrospective {
    private int exhibitid;
    private String dt;
    private Double availability;

    public int getExhibitid() {
        return exhibitid;
    }

    public void setExhibitid(int exhibitid) {
        this.exhibitid = exhibitid;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public Double getAvailability() {
        return availability;
    }

    public void setAvailability(Double availability) {
        this.availability = availability;
    }

    public ExhibitRetrospective(int exhibitid, String dt, Double availability) {

        this.exhibitid = exhibitid;
        this.dt = dt;
        this.availability = availability;
    }
}
