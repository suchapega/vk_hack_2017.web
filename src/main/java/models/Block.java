package models;

public class Block {
    private int id;
    private String block;
    private float availability;
    private int usersamount;

    public Block(int id, String block, float availability, int usersamount) {
        this.id = id;
        this.block = block;
        this.availability = availability;
        this.usersamount = usersamount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public float getAvailability() {
        return availability;
    }

    public void setAvailability(float availability) {
        this.availability = availability;
    }

    public int getUsersamount() {
        return usersamount;
    }

    public void setUsersamount(int usersamount) {
        this.usersamount = usersamount;
    }
}
