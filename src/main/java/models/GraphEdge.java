package models;

public class GraphEdge {
    private int id;
    private int startnodeid;
    private int endnodeid;
    private double weight;

    public GraphEdge(int id, int startnodeid, int endnodeid, double weight) {
        this.id = id;
        this.startnodeid = startnodeid;
        this.endnodeid = endnodeid;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStartnodeid() {
        return startnodeid;
    }

    public void setStartnodeid(int startnodeid) {
        this.startnodeid = startnodeid;
    }

    public int getEndnodeid() {
        return endnodeid;
    }

    public void setEndnodeid(int endnodeid) {
        this.endnodeid = endnodeid;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
