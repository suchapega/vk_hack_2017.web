package models;

public class Hole  {
    private int id;
    private int floor;
    //SELECT id, floor, lefttopx, lefttopy, leftbotx, leftboty, righttopx, righttopy, rightbotx, rightboty, centerx, centery, categoryidid FROM hole
    private Double  lefttopx;
    private Double  lefttopy;
    private Double  leftbotx;
    private Double  leftboty;
    private Double  righttopx;
    private Double  righttopy;
    private Double  rightbotx;
    private Double  rightboty;
    private Double  centerx;
    private Double  centery;
    private int  categoryid;
    private float availability;
    private int usersamount;

    public Hole(int id, int floor, Double lefttopx, Double lefttopy, Double leftbotx, Double leftboty, Double righttopx, Double righttopy, Double rightbotx, Double rightboty, Double centerx, Double centery, int categoryid, float availability, int usersamount) {
        this.id = id;
        this.floor = floor;
        this.lefttopx = lefttopx;
        this.lefttopy = lefttopy;
        this.leftbotx = leftbotx;
        this.leftboty = leftboty;
        this.righttopx = righttopx;
        this.righttopy = righttopy;
        this.rightbotx = rightbotx;
        this.rightboty = rightboty;
        this.centerx = centerx;
        this.centery = centery;
        this.categoryid = categoryid;
        this.availability = availability;
        this.usersamount = usersamount;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public float getAvailability() {
        return availability;
    }

    public void setAvailability(float availability) {
        this.availability = availability;
    }

    public int getUsersamount() {
        return usersamount;
    }

    public void setUsersamount(int usersamount) {
        this.usersamount = usersamount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public Double getLefttopx() {
        return lefttopx;
    }

    public void setLefttopx(Double lefttopx) {
        this.lefttopx = lefttopx;
    }

    public Double getLefttopy() {
        return lefttopy;
    }

    public void setLefttopy(Double lefttopy) {
        this.lefttopy = lefttopy;
    }

    public Double getLeftbotx() {
        return leftbotx;
    }

    public void setLeftbotx(Double leftbotx) {
        this.leftbotx = leftbotx;
    }

    public Double getLeftboty() {
        return leftboty;
    }

    public void setLeftboty(Double leftboty) {
        this.leftboty = leftboty;
    }

    public Double getRighttopx() {
        return righttopx;
    }

    public void setRighttopx(Double righttopx) {
        this.righttopx = righttopx;
    }

    public Double getRighttopy() {
        return righttopy;
    }

    public void setRighttopy(Double righttopy) {
        this.righttopy = righttopy;
    }

    public Double getRightbotx() {
        return rightbotx;
    }

    public void setRightbotx(Double rightbotx) {
        this.rightbotx = rightbotx;
    }

    public Double getRightboty() {
        return rightboty;
    }

    public void setRightboty(Double rightboty) {
        this.rightboty = rightboty;
    }

    public Double getCenterx() {
        return centerx;
    }

    public void setCenterx(Double centerx) {
        this.centerx = centerx;
    }

    public Double getCentery() {
        return centery;
    }

    public void setCentery(Double centery) {
        this.centery = centery;
    }

    public int getcategoryid() {
        return categoryid;
    }

    public void setcategoryid(int categoryid) {
        this.categoryid = categoryid;
    }
}
