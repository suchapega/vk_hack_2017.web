package models;

public class User {
    private int id;
    private String name;
    private String lastname;
    private String url;
    private String usertoken;
    private Double currentpositionx;
    private Double currentpositiony;
    private int floor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getCurrentpositionx() {
        return currentpositionx;
    }

    public void setCurrentpositionx(Double currentpositionx) {
        this.currentpositionx = currentpositionx;
    }

    public Double getCurrentpositiony() {
        return currentpositiony;
    }

    public void setCurrentpositiony(Double currentpositiony) {
        this.currentpositiony = currentpositiony;
    }

    public String getUsertoken() {
        return usertoken;
    }

    public void setUsertoken(String usertoken) {
        this.usertoken = usertoken;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public User(int id, String name, String lastname, String url, String usertoken, Double currentpositionx, Double currentpositiony, int floor) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.url = url;
        this.usertoken = usertoken;
        this.currentpositionx = currentpositionx;
        this.currentpositiony = currentpositiony;
        this.floor = floor;
    }
}
