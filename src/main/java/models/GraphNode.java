package models;

public class GraphNode {
    private int id;
    private boolean ishole;
    private boolean holeid;
    private Double floor;
    private Double posx;
    private Double posy;
    private Double availability;

    public GraphNode(int id, boolean ishole, boolean holeid, Double floor, Double posx, Double posy, Double availability) {
        this.id = id;
        this.ishole = ishole;
        this.holeid = holeid;
        this.floor = floor;
        this.posx = posx;
        this.posy = posy;
        this.availability = availability;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIshole() {
        return ishole;
    }

    public void setIshole(boolean ishole) {
        this.ishole = ishole;
    }

    public boolean isHoleid() {
        return holeid;
    }

    public void setHoleid(boolean holeid) {
        this.holeid = holeid;
    }

    public Double getFloor() {
        return floor;
    }

    public void setFloor(Double floor) {
        this.floor = floor;
    }

    public Double getPosx() {
        return posx;
    }

    public void setPosx(Double posx) {
        this.posx = posx;
    }

    public Double getPosy() {
        return posy;
    }

    public void setPosy(Double posy) {
        this.posy = posy;
    }

    public Double getAvailability() {
        return availability;
    }

    public void setAvailability(Double availability) {
        this.availability = availability;
    }
}
