package models;

public class Exhibit {
    private int id;
    private String name;
    private String url;
    private String img;
    private String author;
    private int categoryid;
    private String collection;
    private String location;
    private int hole;

    public Exhibit(int id, String name, String url, String img, String author, int categoryid, String collection, String location, int hole) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.img = img;
        this.author = author;
        this.categoryid = categoryid;
        this.collection = collection;
        this.location = location;
        this.hole = hole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getcategoryid() {
        return categoryid;
    }

    public void setcategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getHole() {
        return hole;
    }

    public void setHole(int hole) {
        this.hole = hole;
    }
}
