package recommendations;

import algorithms.HoleDetecter;
import jdbcdriver.JDBCDriver;
import models.Exhibit;

import java.util.List;
import java.util.Random;

public class Recommend {
    private String usertoken;
    private Double currentposx;
    private Double currentposy;
    private int floor;
    private int currenthole;

    public Recommend(String usertoken, Double currentposx, Double currentposy, int floor) throws Exception {
        this.usertoken = usertoken;
        this.currentposx = currentposx;
        this.currentposy = currentposy;
        this.floor = floor;
        HoleDetecter hd = new HoleDetecter(usertoken);
        currenthole = hd.Detect();//hd.getHole
    }
    public List<Exhibit> recommendAvailableExhibits() throws Exception {
        JDBCDriver jd = new JDBCDriver();
        return jd.getRecommendedExhibits(usertoken,currenthole);
    }
    public List<Exhibit> recommendLikedExhibits() throws Exception {
        JDBCDriver jd = new JDBCDriver();
        return jd.getLikedExhibits(usertoken,currenthole);
    }
    public List<Exhibit> randomRecommendation() throws Exception {
        Random rand = new Random();
        if(rand.nextBoolean()){
            return recommendAvailableExhibits();
        }else{
            return recommendLikedExhibits();
        }
    }



}
