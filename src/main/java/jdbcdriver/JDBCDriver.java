package jdbcdriver;

import models.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCDriver {
    private String driver;
    private String connectionString;
    private String user;
    private String password;

    public JDBCDriver() throws Exception{
        driver = "com.mysql.jdbc.Driver";
        connectionString = "jdbc:mysql://irkm0xtlo2pcmvvz.chr7pe7iynqr.eu-west-1.rds.amazonaws.com:3306/pdxxkdc2fohymjuw";
        user = "vbyq41tpj85gjxi3";
        password = "yawqeesudui7qzmm";

    }
    private Connection getConnection() throws SQLException,ClassNotFoundException {
        Class.forName(driver);
        Connection con = DriverManager.getConnection(connectionString, user, password);
        return con;
    }
    public List<Exhibit> getExhibits(){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Exhibit> exhibits = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, name, url, img, author, categoryid, collection, location, hole FROM exhibits");
            while (rs.next()) {
                exhibits.add(new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibits;
    }
    public Exhibit getExhibit(int id){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        Exhibit exhibit = null;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, name, url, img, author, categoryid, collection, location, hole FROM exhibit WHERE id = " + Integer.toString(id));

            while (rs.next()) {
                exhibit = new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole"));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibit;
    }
    public List<Exhibit> getExhibitsByCategory(int categoryid){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Exhibit> exhibits = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, name, url, img, author, categoryid, collection, location, hole FROM exhibits WHERE categoryid = " + Integer.toString(categoryid));
            while (rs.next()) {
                exhibits.add(new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibits;
    }
    public List<Category> getCategories(){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Category> category = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, category FROM category");
            while (rs.next()) {
                category.add(new Category(rs.getInt("id"),
                        rs.getString("category")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return category;
    }
    public ArrayList<Hole> getHoles(){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        ArrayList<Hole> hole = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, floor, lefttopx, lefttopy, leftbotx, leftboty, righttopx, righttopy, rightbotx, rightboty, centerx, centery, categoryid, availability, usersamount FROM hole where floor is not null and categoryid is not null and leftbotx is not null");
            while (rs.next()) {
                hole.add(new Hole(rs.getInt("id"),
                        rs.getInt("floor"),
                        rs.getDouble("lefttopx"),
                        rs.getDouble("lefttopy"),
                        rs.getDouble("leftbotx"),
                        rs.getDouble("leftboty"),
                        rs.getDouble("righttopx"),
                        rs.getDouble("righttopy"),
                        rs.getDouble("rightbotx"),
                        rs.getDouble("rightboty"),
                        rs.getDouble("centerx"),
                        rs.getDouble("centery"),
                        rs.getInt("categoryid"),
                        rs.getFloat("availability"),
                        rs.getInt("usersamount")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return hole;
    }
    public Hole getHole(int id){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        Hole hole = null;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, floor, lefttopx, lefttopy, leftbotx, leftboty, righttopx, righttopy, rightbotx, rightboty, centerx, centery, categoryid, availability, usersamount FROM hole where floor is not null and categoryid is not null and leftbotx is not null");
            while (rs.next()) {
                hole = new Hole(rs.getInt("id"),
                        rs.getInt("floor"),
                        rs.getDouble("lefttopx"),
                        rs.getDouble("lefttopy"),
                        rs.getDouble("leftbotx"),
                        rs.getDouble("leftboty"),
                        rs.getDouble("righttopx"),
                        rs.getDouble("righttopy"),
                        rs.getDouble("rightbotx"),
                        rs.getDouble("rightboty"),
                        rs.getDouble("centerx"),
                        rs.getDouble("centery"),
                        rs.getInt("categoryid"),
                        rs.getFloat("availability"),
                        rs.getInt("usersamount"));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return hole;
    }
    public List<Block> getBlocks(){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Block> block = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, block, availability, usersamount FROM block");
            while (rs.next()) {
                block.add(new Block(rs.getInt("id"),
                        rs.getString("block"),
                        rs.getFloat("availability"),
                        rs.getInt("usersamount")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return block;
    }
    public Block getBlock(int id){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        Block block = null;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, block, availability, usersamount FROM block WHERE id = " + Integer.toString(id));
            while (rs.next()) {
                block = new Block(rs.getInt("id"),
                        rs.getString("block"),
                        rs.getFloat("availability"),
                        rs.getInt("usersamount"));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return block;
    }
    public List<Exhibit> getWishlistExhibits (int wishlistid){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Exhibit> exhibits = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT e.id, e.name, e.url, e.img, e.author, e.collection, e.location, e.hole, e.categoryid  FROM wishlist_exhibits as w " +
                    "INNER JOIN exhibit as e " +
                    "ON w.exhibitid = e.id " +
                    "WHERE wishlistid = " + wishlistid);
            while (rs.next()) {
                exhibits.add(new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibits;
    }
    public List<Wishlist> getWishlist (String usertoken){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Wishlist> wishlist = null;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, usertoken, name FROM wishlist WHERE usertoken = '" + usertoken + "'");
            while (rs.next()) {
                wishlist.add(new Wishlist(rs.getInt("id"), usertoken,
                        rs.getString("name")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return wishlist;
    }
    public boolean isLiked (String usertoken, int exhibitid){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        boolean flag = false;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT * FROM likes WHERE usertoken = '" + usertoken + "', exhibitid = " + Integer.toString(exhibitid));
            while (rs.next()) {
                flag = true;
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return flag;
    }
    public int setLike (String usertoken, int exhibitid){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();

            if(isLiked(usertoken, exhibitid)){
                String query = " insert into likes (exhibitid, usertoken)"
                        + " values (?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt (1, exhibitid);
                preparedStmt.setString (2, usertoken);

                // execute the preparedstatement
                preparedStmt.execute();
            } else{
                String query = "delete from likes where exhibitid = ? and usertoken = ?";
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt (1, exhibitid);
                preparedStmt.setString (2, usertoken);

                // execute the preparedstatement
                preparedStmt.execute();

            }

        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return 0;
    }
    public int setLike (String usertoken, int exhibitid, boolean flag){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();

            if(flag){
                String query = " insert into likes (exhibitid, usertoken)"
                        + " values (?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt (1, exhibitid);
                preparedStmt.setString (2, usertoken);

                // execute the preparedstatement
                preparedStmt.execute();
            } else{
                String query = "delete from likes where exhibitid = ? and usertoken = ?";
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt (1, exhibitid);
                preparedStmt.setString (2, usertoken);

                // execute the preparedstatement
                preparedStmt.execute();

            }

        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return 0;
    }
    public int SetUserCurrentPosition (String usertoken, Double x, Double y, int floor){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            stmt.executeUpdate("UPDATE user set currentposx = " + Double.toString(x) + ", currentposy = " + Double.toString(x) + ", currentfloor = " + Integer.toString(floor) + " WHERE usertoken = '" + usertoken + "'");

        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
            return 1;
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                    return 1;
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
            return 1;
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                return 1;
            }

        }
        return 0;
    }
    public int setWishListExhibit (String usertoken, int wishlistid, String wishlistname, int exhibitid){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        //if whishlistid = 0 then create new whishlist, else add new
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();

            if(wishlistid == 0){
                String query = "insert into wishlist (usertoken, name) values (?, ?); " +
                        "insert into wishlist_exhibits (wishlistid, ?) " +
                        "SELECT id, ? FROM wishlist WHERE usertoken = ?, name = ?";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setString (1, usertoken);
                preparedStmt.setString (2, wishlistname);
                preparedStmt.setInt (3, exhibitid);
                preparedStmt.setInt (4, exhibitid);
                preparedStmt.setString (5, usertoken);
                preparedStmt.setString (6, wishlistname);

                // execute the preparedstatement
                preparedStmt.execute();
            } else{
                String query = "INSERT INTO wishlist_exhibits (wishlistid, exhibitid) VALUES (?,?)";
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt (1, wishlistid);
                preparedStmt.setInt (2, exhibitid);

                // execute the preparedstatement
                preparedStmt.execute();

            }

        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }
        }
        return 0;
    }
    public User getUser(String usertoken){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        User user = null;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT id, name, lastname, url, usertoken, currentposx, currentposy, currentfloor FROM user WHERE usertoken = '" + usertoken + "'");
            while (rs.next()) {
                user = new User(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("lastname"),
                        rs.getString("url"),
                        usertoken,
                        rs.getDouble("currentposx"),
                        rs.getDouble("currentposy"),
                        rs.getInt("currentfloor"));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return user;
    }
    public boolean auth (String usertoken){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        boolean flag = false;
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM user WHERE usertoken = '" + usertoken + "'");
            while (rs.next()){
                flag = true;
            }
            if (!flag){
                String query = " insert into user (usertoken)"
                        + " values (?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setString (1, usertoken);

                // execute the preparedstatement
                preparedStmt.execute();
                flag = true;
            }


        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return flag;
    }
    public List<Exhibit> getRecommendedExhibits(String usertoken, int currenthole){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Exhibit> exhibits = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT distinct e.id, e.name, e.url, e.img, e.author, " +
                    "e.collection, e.location, e.hole, e.categoryid " +
                    "FROM hole h " +
                    "INNER JOIN wishlist w on w.usertoken = '"+ usertoken +"' " +
                    "INNER JOIN wishlist_exhibits we on w.id = we.wishlistid " +
                    "INNER JOIN exhibit e on e.id = we.exhibitid AND h.id = e.hole " +
                    "WHERE h.availability < 4.0 AND h.id != "  + Integer.toString(currenthole));
            while (rs.next()) {
                exhibits.add(new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibits;
    }
    public List<Exhibit> getLikedExhibits(String usertoken, int currenthole){
        Connection conn = null; // connection object
        Statement stmt = null; // statement object
        ResultSet rs = null; // result set object
        List<Exhibit> exhibits = new ArrayList<>();
        try{
            conn = getConnection();
            conn.setAutoCommit(true);
            stmt = conn.createStatement();
            // get all of the the records from the cats_tricks table
            rs = stmt.executeQuery("SELECT distinct e.id, e.name, e.url, e.img, e.author, " +
                    "e.collection, e.location, e.hole, e.categoryid " +
                    "FROM hole h " +
                    "INNER JOIN likes l on l.usertoken = '" + usertoken + "' " +
                    "INNER JOIN exhibit e on e.id = l.exhibitid AND h.id = e.hole " +
                    "where h.availability < 4.0 AND h.id != "  + Integer.toString(currenthole));
            while (rs.next()) {
                exhibits.add(new Exhibit(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("url"),
                        rs.getString("img"),
                        rs.getString("author"),
                        rs.getInt("categoryid"),
                        rs.getString("collection"),
                        rs.getString("location"),
                        rs.getInt("hole")));
            }
        } catch (ClassNotFoundException ce) {
            // if the driver class not found, then we will be here
            System.out.println(ce.getMessage());
        } catch (SQLException e) {
            // something went wrong, we are handling the exception here
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

            }
            System.out.println("--- SQLException caught ---");
            // iterate and get all of the errors as much as possible.
            while (e != null) {
                System.out.println("Message   : " + e.getMessage());
                System.out.println("SQLState  : " + e.getSQLState());
                System.out.println("ErrorCode : " + e.getErrorCode());
                System.out.println("---");
                e = e.getNextException();
            }
        }finally { // close db resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
            }

        }
        return exhibits;
    }
}
