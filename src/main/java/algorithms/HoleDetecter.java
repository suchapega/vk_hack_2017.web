package algorithms;

import io.netty.resolver.HostsFileEntries;
import jdbcdriver.JDBCDriver;
import models.Hole;
import models.User;

import java.util.*;

public class HoleDetecter {
    private String token;
    public HoleDetecter(String token) throws Exception{
        this.token = token;
    }

    private int getIndByID(ArrayList<Hole> holes, int ID){
        for (int i = 0; i < holes.size(); i++){
            if (holes.get(i).getId() == ID) return i;
        }
        return -1;
    }
    public List<Integer> YSearch(ArrayList<Hole> holes, Hole key)
    {
        ArrayList<Hole> leftP = new ArrayList<Hole>(holes.size() + 1);
        ArrayList<Hole> rightP = new ArrayList<Hole>(holes.size() + 1);
        leftP.addAll(holes);
        rightP.addAll(holes);
        leftP.add(key);
        rightP.add(key);
        Collections.sort(leftP, new LeftYComparator());
        Collections.sort(rightP, new RightYComparator());

        List<Integer> left = new ArrayList<Integer>();
        List<Integer> right = new ArrayList<Integer>();
        int lind = getIndByID(leftP, key.getId());
        for (int i = 0; i < lind; i++){
            left.add(leftP.get(i).getId());
        }
        int rind = getIndByID(rightP, key.getId());
        for (int i = rind + 1; i < rightP.size(); i++){
            right.add(rightP.get(i).getId());
        }
        left.retainAll(right);
        return left;
    }

    public List<Integer> XSearch(ArrayList<Hole> holes, Hole key)
    {
        ArrayList<Hole> topP = new ArrayList<Hole>(holes.size() + 1);
        ArrayList<Hole> botP = new ArrayList<Hole>(holes.size() + 1);
        topP.addAll(holes);
        botP.addAll(holes);
        topP.add(key);
        botP.add(key);
        Collections.sort(topP, new TopXComparator());
        Collections.sort(botP, new BotXComparator());



        List<Integer> top = new ArrayList<Integer>();
        List<Integer> bot = new ArrayList<Integer>();
        int tind = getIndByID(topP, key.getId());
        int bind = getIndByID(botP, key.getId());

        for (int i = tind + 1; i < topP.size(); i++){
            top.add(topP.get(i).getId());
        }
        for (int i = 0; i < bind; i++){
            bot.add(botP.get(i).getId());
        }
        top.retainAll(bot);
        return top;
    }

    public int Detect() throws Exception {
        System.out.println("holly holes");
        JDBCDriver jd = new JDBCDriver();
        ArrayList<Hole> holes = jd.getHoles();

        User u = jd.getUser(token);
        Hole user;
        user = new Hole(-1, 1, u.getCurrentpositionx(), u.getCurrentpositiony(), u.getCurrentpositionx(),
                u.getCurrentpositiony(),u.getCurrentpositionx(), u.getCurrentpositiony(), u.getCurrentpositionx(),
                u.getCurrentpositiony(), 0.0, 0.0, 0, 0, 0);

        List<Integer> YUserHoles = YSearch(holes, user);
        System.out.println("YUserHoles");
        for (int i = 0; i < YUserHoles.size(); i++) { System.out.println(YUserHoles.get(i)); }

        List<Integer> XUserHoles = XSearch(holes, user);
        System.out.println("XUserHoles");
        for (int i = 0; i < XUserHoles.size(); i++) { System.out.println(XUserHoles.get(i)); }

        YUserHoles.retainAll(XUserHoles);
        Set<Integer> Y = new HashSet<Integer>(YUserHoles);
        Set<Integer> X = new HashSet<Integer>(XUserHoles);
        Y.removeAll(X);
        //List<Integer> out = new ArrayList<Integer>(Y) ;
        return 12;
    }

}

