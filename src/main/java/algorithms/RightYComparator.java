package algorithms;

import models.Hole;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class RightYComparator implements Comparator<Hole> {
    @Override
    public int compare(@NotNull Hole a, Hole b) {
        return a.getRightboty().compareTo(b.getRightboty());
    }
}

