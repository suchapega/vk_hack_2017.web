package algorithms;

import models.Hole;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class TopXComparator implements Comparator<Hole> {
    @Override
    public int compare(@NotNull Hole a, Hole b) {
        return a.getRighttopx().compareTo(b.getRighttopx());
    }
}
