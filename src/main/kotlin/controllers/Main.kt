package controllers

import algorithms.HoleDetecter
import com.sun.org.apache.xml.internal.serializer.OutputPropertyUtils
import jdbcdriver.JDBCDriver
import org.jetbrains.exposed.sql.Database
import org.jetbrains.ktor.netty.*
import RESTmodels.*
import enums.Weight
import org.jetbrains.ktor.routing.*
import org.jetbrains.ktor.application.*
import org.jetbrains.ktor.features.ConditionalHeaders
import org.jetbrains.ktor.features.DefaultHeaders
import org.jetbrains.ktor.features.PartialContentSupport
import java.sql.DriverManager
import org.jetbrains.ktor.response.*

import org.jetbrains.ktor.request.*

//import org.jetbrains.ktor.auth.*


import com.fasterxml.jackson.module.kotlin.*
import enums.GetWeight
import models.Category
import models.Exhibit
import models.Hole
import models.Wishlist
import org.jetbrains.ktor.gson.GsonSupport
import org.jetbrains.ktor.http.*
import java.time.Year


val driver = "com.mysql.jdbc.Driver"
val connection = "jdbc:mysql://irkm0xtlo2pcmvvz.chr7pe7iynqr.eu-west-1.rds.amazonaws.com:3306/pdxxkdc2fohymjuw"
val user = "vbyq41tpj85gjxi3"
val password = "yawqeesudui7qzmm"



fun Application.main() {
    install(DefaultHeaders)
    install(ConditionalHeaders)
    install(PartialContentSupport)
    install(GsonSupport)
    install(Routing) {

        val dbDriver = JDBCDriver()

        val mapper = jacksonObjectMapper()

        //<editor-fold desk="test REST">

        get("/") {
            call.respond(HoleDetecter("usertoken").Detect())

        }

        get("/test") {
            call.respond("Lubudabdudaap")
        }

        get("error") {
            throw IllegalStateException("An invalid place to be …")
        }
        get("/jdbc"){
            var output = "";
            for(cate in JDBCDriver().holes){
                output = output + cate.leftbotx + " "
            }
            call.respond("DbConnect:${output}")
        }


        get("/testDbConnect") {

            Class.forName(driver)
            val con = DriverManager.getConnection(connection, user, password)

            if(!con.isClosed){

                val stmt = con.createStatement()

                val sql = "SELECT Id, Name FROM Exhibit"
                val rs = stmt.executeQuery(sql)

                var output = ""
                while (rs.next()) {

                    val id = rs.getInt("id")
                    val name = rs.getString("name")

                    output = output.plus("\n${id}: ${name}")
                }
                con.close()

                call.respond("DbConnect:${output}")
            }else{
                call.respond("DbNotConnect")
            }
        }

        //</editor-fold>

        //<editor-fold desk="hard REST">

        //+1 Запрос на авторизацию/регистрацию нового пользователя
        post("/auth"){

            val data = call.receive<AuthorizationData>()

            if(dbDriver.auth(data.user_token)){
                call.respond(HttpStatusCode.OK)
            }else{
                call.respond(HttpStatusCode.BadRequest)
            }

        }

        //+2 Запрос на обновление текущей геопозиции
        post("/update_position"){

            val data = call.receive<PositionDate>()

            try {
                dbDriver.SetUserCurrentPosition(data.user_token, data.position.latitude, data.position.longitude, data.position.floor)
                call.respond(HttpStatusCode.OK)
            }
            catch (e: Exception){
                call.respond(HttpStatusCode.BadRequest)
            }
        }

        //+3 Запрос на общую степень загруженности
        get("/weight") {

           val weightD = dbDriver.getBlock(1).availability

           var jsonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(WeightWrapper(GetWeight(weightD)))
           call.respond(jsonStr)

        }

        //+4 Запрос на загруженность залов
        get("/weight/rooms") {

            val holes = dbDriver.getHoles()

            val roomDataToRestList = mutableListOf<RoomData>()

            for (item: Hole in holes) {

                roomDataToRestList.add(RoomData(item.id, Position(item.floor, item.centerx, item.centery),
                        listOf(Pair(item.leftbotx, item.leftboty),Pair(item.lefttopx, item.lefttopy),Pair(item.righttopx, item.righttopy),Pair(item.rightbotx, item.rightboty)),GetWeight(item.availability)))
            }

            var jsonStr = mapper.writeValueAsString(roomDataToRestList)

            call.respond(jsonStr)
        }

        //5 ???

        //+6 Запрос на получение категорий
        get("/categories") {

            val categories = dbDriver.getCategories()

            val categoriesDataToJSONList = mutableListOf<CategoryData>()

            for (item: Category in categories) {
                categoriesDataToJSONList.add(CategoryData(item.id, item.category))
            }

            var jsonStr = mapper.writeValueAsString(categoriesDataToJSONList)

            call.respond(jsonStr)
        }

        //+7 Запрос на получение коллекции экспонатов
        get("/exhibits/{id}") {

            val categoryId =  call.parameters["id"].toString()

            val exhibits = dbDriver.getExhibitsByCategory(categoryId.toInt())

            val exhibitsDataToJSONList = mutableListOf<ExhibitData>()

            for (item: Exhibit in exhibits) {
                exhibitsDataToJSONList.add(ExhibitData(item.id, item.name, item.author, item.url))
            }

            var jsonStr = mapper.writeValueAsString(exhibitsDataToJSONList)

            call.respond(jsonStr)
        }

        //+8 Запрос на один экспонат
        post("/exhibit"){

            val exhibitPostData = call.receive<ExhibitPostData>()

            val exhibit = dbDriver.getExhibit(exhibitPostData.exhibit_id)
            val isLike = dbDriver.isLiked(exhibitPostData.user_token, exhibitPostData.exhibit_id)
            var hole = dbDriver.getHole(exhibit.hole)

            var exhibitToJSON = ExhibitJ(exhibit.id, exhibit.name, exhibit.author, exhibit.url, exhibit.collection, "", Year.of(0), "", "", "",
                    RoomData(exhibit.hole, Position(hole.floor, hole.centerx, hole.centery),
                            listOf(Pair(hole.leftbotx, hole.leftboty),
                                   Pair(hole.lefttopx, hole.lefttopy),
                                   Pair(hole.righttopx, hole.righttopy),
                                   Pair(hole.rightbotx, hole.rightbotx)), GetWeight(hole.availability)), isLike )


            var jsonStr = mapper.writeValueAsString(exhibitToJSON)

            call.respond(jsonStr)
        }

        //9 ???

        //+10 Запрос на лайк экземпляра
        post("/like"){

            val data = call.receive<LikeData>()

            try{
                dbDriver.setLike(data.user_token,data.exhibit_id, data.like)
                call.respond(HttpStatusCode.OK)
            }
            catch (e : Exception){
                call.respond(HttpStatusCode.BadRequest)
            }

        }

//        //11 Запрос на получение рекомендаций
//        post("/recommendation"){
//
//            val data = call.receive<AuthorizationData>()
//
//            //val dbDriver.randomRecommendation()
//
//            //val res = ExhibitData(47,"qwwer","yjhtgf")
//
//            val mapper = jacksonObjectMapper()
//
//            var jsonStr = mapper.writeValueAsString(res)
//
//            call.respond(jsonStr)
//        }

        //12 Запрос на добавление экспоната в вишлист
        post("/wishlistExhibit/add"){

            val wishlistData = call.receive<WishlistData>()

            try{
                dbDriver.setWishListExhibit(wishlistData.user_token, wishlistData.wishlist_id, wishlistData.wishlist_name, wishlistData.exhibit_id)
                call.respond(HttpStatusCode.OK)
            }
            catch (e: Exception){
                 call.respond(HttpStatusCode.BadRequest)
            }


        }

        //+13 Запрос на получение вишлистов
        post("/wishlists"){

            val data = call.receive<AuthorizationData>()

            var wishlists = dbDriver.getWishlist(data.user_token)

            val wishlistsDataToJSONList = mutableListOf<WishlistShortData>()

            for (item: Wishlist in wishlists) {
                wishlistsDataToJSONList.add(WishlistShortData(item.id, item.getname()))
            }

            var jsonStr = mapper.writeValueAsString(wishlistsDataToJSONList)

            call.respond(jsonStr)
        }

        //+14 Запрос на получение экспонатов в вишлисте
        post("/wishlistExhibits"){

            val data = call.receive<WishlistShort2Data>()

            var wishlistExhibits = dbDriver.getWishlistExhibits(data.wishlist_id)

            val valwishlistExhibitsToJSONList = mutableListOf<ExhibitData>()

            for (item: Exhibit in wishlistExhibits) {

                valwishlistExhibitsToJSONList.add(ExhibitData(item.id, item.name, item.author, item.url))
            }

            var jsonStr = mapper.writeValueAsString(valwishlistExhibitsToJSONList)

            call.respond(jsonStr)
        }
        //</editor-fold>
    }
}