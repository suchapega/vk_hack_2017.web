package enums

const val MaxWeight = 10.0

fun GetWeight(availability: Float) : Weight
{
    var weight = Weight.Unknown

    if((availability > 0.0) and (availability <= enums.MaxWeight / 3.0)){

        weight = Weight.Low
    }

    if((availability > enums.MaxWeight / 3.0) and (availability <= 2 * enums.MaxWeight / 3.0)){

        weight = Weight.Medium
    }

    if((availability > 2 * enums.MaxWeight / 3.0) and (availability <= enums.MaxWeight)){

        weight = Weight.Medium
    }

    return weight
}

enum class Weight(val value: Int) {
    Unknown(0), Low(1), Medium(2), Hight(3)
}