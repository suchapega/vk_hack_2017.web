package RESTmodels

data class PositionDate (val user_token: String, val position: Position)

data class Position (val floor: Int, val latitude: Double, val longitude: Double)

