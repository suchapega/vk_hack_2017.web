package RESTmodels

import enums.Weight

data class WeightWrapper(val weigh: Weight)