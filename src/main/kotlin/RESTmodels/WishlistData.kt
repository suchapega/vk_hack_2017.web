package RESTmodels

data class WishlistData (val user_token: String, val wishlist_id : Int, val wishlist_name: String , val exhibit_id : Int)

data class WishlistShortData (val id: Int, val Name: String)

data class WishlistShort2Data (val user_token: String, val wishlist_id: Int)

