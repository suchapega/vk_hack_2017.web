package RESTmodels

data class LikeData (val user_token: String, val exhibit_id: Int, val like: Boolean)