package RESTmodels

data class AuthorizationData(val user_token: String)