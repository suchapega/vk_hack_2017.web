package RESTmodels

data class CategoryData (val id: Int, val name: String)