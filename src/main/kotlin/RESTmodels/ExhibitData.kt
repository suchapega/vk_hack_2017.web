package RESTmodels

import java.time.Year

data class ExhibitData (val id: Int, val name: String, val author: String, val image_url : String)

data class ExhibitPostData (val user_token: String, val exhibit_id : Int)

data class ExhibitJ(val id: Int, val name: String, val author: String, val image_url: String, val collection: String, val place: String, val year: Year, val material: String, val technique: String, val description: String, val room: RoomData, val like: Boolean)