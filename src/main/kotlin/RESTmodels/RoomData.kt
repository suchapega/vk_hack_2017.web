package RESTmodels

import enums.Weight
import java.awt.Point

data class RoomData(val number: Int, val position: Position, val corners: List<Pair<Double, Double>>, val weight: Weight)